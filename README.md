# Tutoriels LP CISIIE

## module html avancé

Construire un tutoriel basé sur une démo sur un des thèmes listés ci-dessous.
Chaque tutoriel correspond à un des répertoires du projet.
Le tutoriel en lui-même, ou un lien, doit être décrit dans le fichier README.md du répertoire, en utilisant la syntaxe markdown.


## thèmes prévus : 
1. css transition : Barbier, Vuillaume, Champion 
2. css gradients : Cordier, Rimet, Paradis
3. css color module, level 3 : Altomonte, Chognard, Wittmann
4. filter effects : Mangin, Legougne
5. multiple column layout : Rio, Guillaume, Logeard
6. blending mode : Bergeret, Bounajra, Iablioune
7. css animation : Richard, Schweitzer, Breuil
8. css transform : Dalençon, Liger, Petitcolin
9. @font-face & webfonts : De Moliner, Demougin, Kausuo

## Ce qui est attendu : 
Sur chaque module, 
1. 1 rappel et un pointeur vers la spécification W3C et une référence de qualité et complète
2.1 tutoriel expliquant l'intéret et l'utilisation du module css, basé sur une sémo
3. la démo elle-même, avec les sources
4. un point sur l'implantation dans les navigateurs
5. une liste de ressources/exemples/tutoriels existant sur le même thème



## Utilisation du dépôt

**Ne travaillez pas directement sur ce dépôt**

Faites un fork pour créer un dépôt dont vous êtes propriétaire.

Utilisez ce nouveau dépôt : clonez-le sur votre machine. 

**Créez une branche portant le nom de votre tuto !**

Faites vos commits/push sur votre dépôt. Quand votre tutoriel est prêt, faites un pull request pour l'intégrer dans ce dépôt.


